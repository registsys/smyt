from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import JsonResponse
from django.shortcuts import render

from product.models import TotalDiscount, Product


def products(request):

    result = TotalDiscount.objects.filter(
        price__gte=1000,
        price__lte=10000
    ).values('product_id', 'name', 'product_discount', 'category_discount',
             'brand_discount', 'total_cost', 'price')

    if request.GET.get('sort') == 'desc':
        result = result.order_by('-total_cost')
    else:
        result = result.order_by('total_cost')

    paginator = Paginator(result, 25)

    page = request.GET.get('page')
    try:
        result = paginator.page(page)
    except PageNotAnInteger:
        result = paginator.page(1)
    except EmptyPage:
        result = paginator.page(paginator.num_pages)

    if request.GET.get('format') == 'json':
        return JsonResponse({'result': list(result)})

    return render(request, 'catalog.html', {'result': list(result), 'num_pages': result.paginator.num_pages})


def products_alternate(request):

    result = Product.objects.catalog()

    if request.GET.get('sort') == 'desc':
        result = result.order_by('-total_cost')
    else:
        result = result.order_by('total_cost')

    paginator = Paginator(result, 25)

    page = request.GET.get('page')
    try:
        result = paginator.page(page)
    except PageNotAnInteger:
        result = paginator.page(1)
    except EmptyPage:
        result = paginator.page(paginator.num_pages)

    if request.GET.get('format') == 'json':
        return JsonResponse({'result': list(result)})

    return render(request, 'catalog.html', {'result': list(result), 'num_pages': result.paginator.num_pages})
