# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
import random
from datetime import datetime
import pytz

from django.conf import settings

from product.models import Product, Category, Brand, Discount


def fill_db():

    begin_promo = pytz.timezone(settings.TIME_ZONE).localize(datetime(year=2017, month=1, day=5), is_dst=None)
    end_promo = pytz.timezone(settings.TIME_ZONE).localize(datetime(year=2017, month=1, day=31), is_dst=None)
    b_sony, _ = Brand.objects.get_or_create(name='sony')
    b_apple, _ = Brand.objects.get_or_create(name='apple')
    b_levis, _ = Brand.objects.get_or_create(name='levis')
    b_nike, _ = Brand.objects.get_or_create(name='nike')
    c_electronic, _ = Category.objects.get_or_create(name='electronic')
    c_wear, _ = Category.objects.get_or_create(name='wear')
    for i in range(1, 10001):
        Product.objects.create(name='tv %s' % i, price=random.random()*20000, brand=b_sony, category=c_electronic)
    for i in range(1, 10001):
        Product.objects.create(name=u'phone %s' % i, price=random.random()*20000, brand=b_apple, category=c_electronic)
    for i in range(1, 10001):
        Product.objects.create(name=u'jeans %s' % i, price=random.random()*20000, brand=b_levis, category=c_wear)
    for i in range(1, 10001):
        Product.objects.create(name=u'jacket %s' % i, price=random.random()*20000, brand=b_nike, category=c_wear)
    for i in range(1, 10001):
        Product.objects.create(name=u'cap %s' % i, price=random.random()*20000, category=c_wear)
    for i in range(1, 10001):
        Product.objects.create(name=u'iron %s' % i, price=random.random()*20000, category=c_electronic)
    d_p1 = Discount(in_percents=20, item=Product.objects.get(pk=1), begin=begin_promo, end=end_promo)
    d_p1.save()
    d_p2 = Discount(in_percents=45, item=Product.objects.get(pk=2), begin=begin_promo, end=end_promo)
    d_p2.save()
    d_c1 = Discount(in_percents=30, item=c_electronic, begin=begin_promo, end=end_promo)
    d_c1.save()
    d_c2 = Discount(in_percents=55, item=c_wear, begin=begin_promo, end=end_promo)
    d_c2.save()
    d_b1 = Discount(in_percents=40, item=b_apple, begin=begin_promo, end=end_promo)
    d_b1.save()
    d_b2 = Discount(in_percents=50, item=b_levis, begin=begin_promo, end=end_promo)
    d_b2.save()


fill_db()
