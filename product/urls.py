from django.conf.urls import url

from product.views import products, products_alternate

urlpatterns = [
    url(r'^$', products, name='list'),
    url(r'^alt/$', products_alternate, name='list_alt'),
]
