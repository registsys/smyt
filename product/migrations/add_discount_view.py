# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL('''CREATE OR REPLACE VIEW total_discounts AS
          SELECT
          row_number() OVER () as id,
          id product_id,
          "name",
          price,
          brand_id,
          category_id,
          product_discount,
          category_discount,
          brand_discount,
          GREATEST(product_discount, category_discount, brand_discount) as max_discount,
          (price - price*GREATEST(product_discount, category_discount, brand_discount)/100)::NUMERIC(10, 2) as total_cost
        FROM (
          SELECT
            p.*,
            coalesce(pd.in_percents, 0) as product_discount,
            coalesce(cd.in_percents, 0) as category_discount,
            coalesce(bd.in_percents, 0) as brand_discount
          FROM product_product AS p
            LEFT OUTER JOIN product_category AS c ON (p.category_id = c.id)
            LEFT OUTER JOIN product_brand AS b ON (p.brand_id = b.id)
            LEFT OUTER JOIN (
              SELECT *
              FROM product_discount
              WHERE content_type_id = (SELECT id FROM django_content_type WHERE app_label='product' and model='product')
              AND now() BETWEEN "begin" and "end"
            ) AS pd ON (pd.object_id = p.id)
            LEFT OUTER JOIN (
              SELECT *
              FROM product_discount
              WHERE content_type_id = (SELECT id FROM django_content_type WHERE app_label='product' and model='category')
              AND now() BETWEEN "begin" and "end"
            ) AS cd ON (cd.object_id = p.category_id)
            LEFT OUTER JOIN (
              SELECT *
              FROM product_discount
              WHERE content_type_id = (SELECT id FROM django_content_type WHERE app_label='product' and model='brand')
              AND now() BETWEEN "begin" and "end"
            ) AS bd ON (bd.object_id = p.brand_id)
          ) total ''')
    ]
