# coding: utf-8
from __future__ import unicode_literals

from datetime import datetime

from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation


class Discount(models.Model):
    limit = models.Q(app_label='product', model='product') \
            | models.Q(app_label='product', model='category') \
            | models.Q(app_label='product', model='brand')
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,
                                     limit_choices_to=limit)
    object_id = models.PositiveIntegerField()
    item = GenericForeignKey('content_type', 'object_id')

    in_percents = models.PositiveSmallIntegerField('размер скидки, %', default=0)
    begin = models.DateTimeField('действует с', default=datetime.now, blank=True)
    end = models.DateTimeField('действует по', default=datetime.now, blank=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if not isinstance(self.item, (Product, Category, Brand, )):
            raise Exception('You can add discount only for product, category or brand.')

        super(Discount, self).save(force_insert, force_update, using, update_fields)


class Category(models.Model):
    name = models.CharField('группа товаров', max_length=64)
    discount = GenericRelation(Discount, related_query_name='category')


class Brand(models.Model):
    name = models.CharField('бренд', max_length=64)
    discount = GenericRelation(Discount, related_query_name='brand')


class CatalogManager(models.Manager):

    def catalog(self):
        from django.db.models import Case, When, F, Q, Value, IntegerField, DecimalField
        from django.db.models.functions import Greatest, Now, Cast

        p_date_filter = Q(discount__end__gte=Now()) & Q(discount__begin__lte=Now())
        c_date_filter = Q(category__discount__end__gte=Now()) & Q(category__discount__begin__lte=Now())
        b_date_filter = Q(brand__discount__end__gte=Now()) & Q(brand__discount__begin__lte=Now())

        return self.get_queryset().filter(
            price__gte=1000,
            price__lte=10000
        ).annotate(
            product_discount=Case(
                When(p_date_filter, then=F('discount__in_percents')), default=Value(0), output_field=IntegerField(),
            )
        ).annotate(
            category_discount=Case(
                When(c_date_filter, then=F('category__discount__in_percents')), default=Value(0),
                output_field=IntegerField(),
            )
        ).annotate(
            brand_discount=Case(
                When(b_date_filter, then=F('brand__discount__in_percents')), default=Value(0),
                output_field=IntegerField(),
            )
        ).annotate(
            total_cost=Cast(
                F('price') * (1 - Greatest('product_discount', 'category_discount', 'brand_discount') / 100.0),
                DecimalField(max_digits=10, decimal_places=2)
            )
        ).values('id', 'name', 'product_discount', 'category_discount',
                 'brand_discount', 'total_cost', 'price')


class Product(models.Model):
    category = models.ForeignKey(Category, verbose_name='группа товара', null=True,
                                 blank=True, on_delete=models.SET_NULL)
    brand = models.ForeignKey(Brand, verbose_name='группа', null=True,
                              blank=True, on_delete=models.SET_NULL)
    name = models.CharField('название товара', max_length=128)
    price = models.DecimalField('стоимость единицы, руб.', max_digits=10, decimal_places=2, default=0)
    discount = GenericRelation(Discount, related_query_name='product')

    objects = CatalogManager()


class TotalDiscount(models.Model):
    """ Модель представлена как одно из возможных решений. Решение заключается в добавлении представления(вьюхи) в бд и
    написание необслуживаемой модели(TotalDiscount) для получения данных из вьюхи средствами ORM. Если сделать Explain
    для запроса на создание вьюхи и получения данных из нее, то Postgres утверждает, что вьюха будет работать до 2х раз
    быстрее, чем решение на чистом ORM (без добавления вьюхи и необслуживаемой модели). НО! запрос через ORM выполняются
    быстрее(процентов на 15-20), чем с использованием данной модели(Explain врет?). Еще, данное решение будет труднее
    сопровождать - менять миграции в случае изменения структуры или типа БД, потому данный способ получения
    каталога стоит рассматривать как альтернативный.(возможно, он имеет право на жизнь при более сложных условиях
    группировок и фильтраций)"""

    id = models.BigIntegerField(primary_key=True)
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    name = models.CharField('название товара', max_length=128)
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING)
    brand = models.ForeignKey(Brand, on_delete=models.DO_NOTHING)
    price = models.DecimalField('стоимость единицы, руб.', max_digits=10, decimal_places=2)
    product_discount = models.PositiveSmallIntegerField('скидка на товар, %')
    category_discount = models.PositiveSmallIntegerField('скидка на группу товаров, %')
    brand_discount = models.PositiveSmallIntegerField('скидка на брэнд, %')
    total_cost = models.DecimalField('стоимость с учетом максимальной скидки, руб.', max_digits=10, decimal_places=2)

    class Meta:
        managed = False
        db_table = 'total_discounts'