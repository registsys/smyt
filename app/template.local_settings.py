import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECRET_KEY = 'hsqrbo2#@hkls(932y48z270ovz1funnluf1f1k&govag6y+qn'
DEBUG = True
ALLOWED_HOSTS = ['*']
INTERNAL_IPS = ['127.0.0.1']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'test',
        'USER': 'user',
        'PASSWORD': 'pass',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    },
}
