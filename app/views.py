from django.shortcuts import render, redirect
from django.urls import reverse


def root(request):

    return redirect(reverse('products:list'))
