from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin

from app.views import root
from product import urls as product_urls

urlpatterns = [
    url(r'^$', root, name='root'),
    url(r'^products/', include(product_urls, namespace='products')),
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
